import java.util.Random;

public class MainSort {

    public static void main(String[] args) {

        Random random = new Random();

        int arraySize = random.nextInt(1000);
        int[] array = new int[arraySize];
        for (int x=0;x<arraySize;x++) {
            array[x] = random.nextInt(1000);
        }
        System.out.println("Unsorted array:");
        for (int p:array) {
            System.out.print(p + " ");
        }

        System.out.println();
        System.out.println("Sorted array:");

        quickSorting(array, 0, array.length - 1);

        for (int i:array) {
            System.out.print(i + " ");
        }
    }

    private static void quickSorting(int[] array, int start, int end) {

        if (end <= start) return;

        int pivot = partition(array, start, end);
        quickSorting(array, start, pivot - 1);
        quickSorting(array, pivot + 1, end);
    }

    private static int partition(int[] array, int start, int end) {

        int pivot = array[end];
        int i = start - 1;

        for (int j=start;j<=end-1;j++) {
            if (array[j] < pivot) {
                i++;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        i++;
        int temp = array[i];
        array[i] = array[end];
        array[end] = temp;

        return i;
    }
}
